@echo off
cat ext/jquery/jquery-1.7.2.min.js ext/jquery/jquery.colorhelpers.js > ./static/jquery-1.7.2.min.js
cat ext/markdown-js/lib/markdown.js > ./static/markdown.js
cat ext/CodeMirror2/lib/codemirror.js ext/CodeMirror2/mode/xml/xml.js ext/CodeMirror2/mode/markdown/markdown.js > ./static/codemirror.js
rem cat ext/markdown-js/lib/markdown.js | uglifyjs -o ./static/markdown.js
rem cat ext/CodeMirror2/lib/codemirror.js ext/CodeMirror2/mode/markdown/markdown.js | uglifyjs -o ./static/codemirror.js
cat ext/normalize.css/normalize.css ext/CodeMirror2/lib/codemirror.css ext/entypo/stylesheet.css > ./static/base.css

cp ext/entypo/entypo* ./static
