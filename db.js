// couchdb client configure
var assert = require('assert');
var cradle = require('cradle');
var async = require('async');

var couch = new (cradle.Connection)({
  auth : { username:'ivory', password:'shiori'}
});

var _users = couch.database('_users');

function logcb(err, res) {
  console.log(err);
  console.log(res);
}

function nothing() {
}


function hit() {
  couch.info(function(err, res){
    if (res.couchdb === 'Welcome')
      console.log('couchdb is up.');
    else {
      console.log('couchdb connection failed.');
      process.exit(1);
    }
  });
}

var prefix = 'org.couchdb.user:';
function create_user(username, password, callback) {
  callback = callback || logcb;
  _users.get(prefix+username, function(err, doc){
    if (err && err.error === 'not_found') {
      // ok to create
      _users.save(prefix+username, {
        name : username,
        password : password,
        type : 'user',
        roles : []
      }, function(err, res) {
        // then create database
        if (err) callback(err);
        var db = couch.database(username);
        db.create(function(err, res){
          if (err) callback(err);
          // set admin and add validations
          // TODO do not assume all will sucess
          return db.put('_security', {
            admins  : { names : [username] },
            readers : { names : [username] }
          }, callback);
        });
      });
    } else {
      callback({error : 'user already exist. / or something else'});
    }
  });
}

// helper function to purge a user and its database
function purge_user(username, callback) {
  _users.get(prefix+username, function(err, doc) {
    _users.remove(prefix+username, doc._rev, nothing);
  });
  var db = couch.database(username).destroy(nothing);
  callback();
}

// do all exports
exports.hit = hit;
exports.create_user = create_user;

// main
if (require.main === module) {
  hit();

  async.series([
      function(callback) {
        create_user('wtfnouser', '123123', callback);
      },
      function(callback) {
        create_user('lilo', '123123', callback);
      },
      function(callback) {
        purge_user('wtfnouser', callback);
      },
      function(callback) {
        purge_user('lilo', callback);
      }
  ], function(err) {
    if (err) {
      console.log('test error');
      console.log(err);
    } else {
      console.log('done!');
    }
  });
  //create_user_async('lilo', '123123');
  //purge_user('wtfnouser');
}
