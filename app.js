// server is basically stateless since it doesn't 
// hit the db. what we do here is set parameters
// based on routing and send them to client by
// setting cookies.
var connect = require('connect'),
         fs = require('fs'),
         db = require('./db.js'),
      utils = require('./utils.js');

// app init
// very naive templating and cache templates in memory
var lrform_tag = "<!-- login-register -->";
lrform = fs.readFileSync('./login-register.html', 'utf8');
template_editor = fs.readFileSync('./editor.html', 'utf8').replace(lrform_tag, lrform);

function forbidden(res, line) {
  var error = {error: line || 'Forbidden'};
  error = JSON.stringify(error);
  res.writeHead(403, {
    'Content-Length' : error.length,
    'Content-Type' : 'text/html'
  });
  res.end(error);
}

function editor(req, res) {
  // TODO test for user in req
  res.writeHead(200, {
    'Content-Length' : template_editor.length,
    'Content-Type' : 'text/html'
  });
  res.end(template_editor);
}

function register(req, res) {
  if (req.method !== 'POST')
    return forbidden(res);

  var body = req.body;
  if (!body || !body.user || !body.password || !body.password2)
    return forbidden(res, 'invalid_form');

  if (body.password != body.password2)
    return forbidden(res, 'pw_mismatch');

  // verified, create user
  db.create_user(body.user, body.password, function(err, result) {
    if (err) forbidden(res, err.error || 'unknown error');
    res.end(JSON.stringify(result));
  });
}

// routes are in matching order
var routes = {
  '/' : null, // main
  '/register'  : register, // register page
  '/writing' : editor,
  '/:user' : null, // listing
  '/:user/:article' : editor
};

// kick off
var app = connect()
  .use(connect.favicon()) // TODO maybe should serve using nginx
  .use(connect.logger('dev'))
  .use(connect.bodyParser())
  .use('/static', connect.static(__dirname + '/static'))
  .use(utils.router(routes))
  .listen(3000);

